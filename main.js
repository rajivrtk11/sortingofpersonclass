class Person {
    constructor(name,salary, age, sex) {
      this.name = name;
      this.salary = salary;
      this.sex = sex;
      this.age = age;
    }
    
    // 0 => name , 1 => salary, 2 => sex, 3 => age
    // true=> ascending, false => descending
     sort(arr, name, order) {
        
        if(name === "salary"){
            if(order == true){
                arr.sort((a,b)=>{
                    return a.salary - b.salary;
                })
            }
            else{
                arr.sort((a,b)=>{
                    return b.salary-a.salary;
                })
            }
        }
        else if(name === "age"){
            if(order == true){
                arr.sort((a,b)=>{
                    return a.age - b.age;
                })
            }
            else{
                arr.sort((a,b)=>{
                    return b.age-a.age;
                })
            }
        }
        else if(name === "name"){
            if(order == true){
                arr.sort((a,b)=>{
                    return a.name.localeCompare(b.name);
                })
            }
            else{
                arr.sort((a,b)=>{
                    return b.name.localeCompare(a.name);
                })
            }
        }
        
    }

}

// name,age, salary, sex
let person1 = new Person("rajiv", 12, 12345, "male");
arr  = [new Person("abhishek", 123450, 10, "male"), new Person("belal", 123451, 11, "male"), new Person("chandu", 123452, 12, "male"),
        new Person("diwakar", 123453, 13, "male"), new Person("eliana", 123454, 14, "male"), new Person("fatima", 123455, 15, "female"),
        new Person("govinda", 123456, 16, "male"), new Person("harish", 123457, 17, "male"), new Person("imran", 123458, 18, "male"),
        new Person("jatin", 123459, 19, "male") ] ;

// person1.sort(arr, true)
console.log(person1.sort(arr,"name", false));
console.log(arr);

